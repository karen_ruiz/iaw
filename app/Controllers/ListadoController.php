<?php

namespace App\Controllers;

use App\Models\Listadomodels;

class ListadoController extends BaseController {

    public function index($valor ="") {
        $matriculados = new Listadomodels();
        $titulo['titulo'] = "Listado Solicitudes";
        $listado['matriculados'] = $matriculados->SELECT("pau.nombre ,pau.apellido1, pau.apellido2, pau.email, tipo_tasa, ciclo.nombre")
                ->join('ciclo', 'pau.ciclo = ciclos.nombre', 'LEFT')
                ->findAll();

        echo view('Listadovista', $listado);
    }

}
