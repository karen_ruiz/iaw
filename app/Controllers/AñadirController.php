<?php

    //Ésta es la carpeta dónde se almacena el nuevo Controlador.
    namespace App\Controllers;

    //Aqui lo seleccionamos para enlazarlo con el Modelo.
    use App\Models\Listadomodels;

    /* Después añadimos la clase con el nuevo controlador
    y lo enlazamos con la base de datos 'centro' y a su vez la tabla 'alumnos'.
    También se tiene que enlazar a su vez con el Modelo y la Vista que hemos creado. */
    class AñadirController extends BaseController {

        public function index() 
        {
           $matriculado = new Listadomodels();
           $lista ['matriculado'] = $matriculado->findAll();
           echo view('añadirvista', $lista);
        }
        
         public function insertar() {

        $model = new Añadirmodel();
        $matriculado = [
            "NIF" => $this->request->getPost('nif'),
            "nombre" => $this->request->getPost('nombre'),
            "apellido1" => $this->request->getPost('apellido1'),
            "apellido2" => $this->request->getPost('apellido2'),
            "email" => $this->request->getPost('email'),
            "ciclo" => $this->request->getPost('ciclo'),
            
        ];

        //$model->insert($alumno);
        print_r($matriculado);
        //echo " ";
        //echo "Acabo de insertar al alumno con todos los datos que querías, ve a la tabla, actualiza y compruébalo";
    }
    }