<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
         <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>

        <title>FORMULARIO</title>
    </head>

    <style>
            .form-group {
                margin-left: 40%;
                margin-bottom: 10px;
            }
            .boton{
                margin-left: 48%;
            }
            .form-control:invalid{
            color:red;    
            }
       
    </style>
 <?php if (!empty(\Config\Services::validation()->getErrors())):?>
<div class="alert alert-danger" role="alert">
    <?= \Config\Services::validation()->listErrors(); ?>
</div>
<?php endif; ?>
    
<?= form_open(site_url('AñadirController/insertar'),['class'=>"needs-validation"]) ?>
    <br>

  <div class="form-group row">
	<!--<label for="NIA" class="col-2 col-form-label">NIA</label>-->
        <div class="col-md-4">
            <span id="NIA">NIE o NIF</span>       
            <?= form_input('NIA',set_value('NIF'),['placeholder'=>"Pon tu NIE o NIF",  'required'=>"required", 'class'=>"form-control", 'maxlength' => '8',])?>
	</div>
  </div>
 

  <div class="form-group row">
	<div class="col-4">
            <span id="nombre">Nombre</span>
  	<?= form_input('nombre',set_value('nombre'),['placeholder'=>"Nombre", 'required'=>"required", 'class'=>"form-control" ])?>
	</div>
  </div>
 
 
 
  <div class="form-group row">
	<div class="col-4">
          <span id="apellido1">Apellido 1</span>
  	<?= form_input('apellido1',set_value('apellido1'),['placeholder'=>" Primer Apellido", 'required'=>"required", 'class'=>"form-control" ])?>
	</div>
  </div>
 
 
  <div class="form-group row">
	<div class="col-4">
            <span id="apellido2">Apellido 2</span>
        <?= form_input('apellido2',set_value('apellido2'),['placeholder'=>"Segundo Apellido",'class'=>"form-control" ])?>
	</div>
  </div>
 

 <div class="form-group row">
	<div class="col-4">
            <span id="nacimiento">Fecha de fin de estudios</span>
	<?= form_input('fin_estudios',set_value('nacimiento'),['placeholder'=>"Fecha de fin de estudios",  'class'=>"form-control" ])?>

	</div>
  </div> 
 
  <div class="form-group row">
	<div class="col-4">
            	<span id="email"> email</span>
<?= form_input('email',set_value('email'),['placeholder'=>"Email",  'class'=>"form-control" ])?>           
	</div>

      <div class="form-group row">
	<div class="col-4">
            	<span id="ciclo"> Ciclo</span>
<?= form_input('ciclo',set_value('ciclo'),['placeholder'=>"ciclo",  'class'=>"form-control" ])?>           
	</div>
      
          <div>
              <p>Select a maintenance drone:</p>

<div>
  <input type="radio" id="huey" name="1" value="1"
         checked>
  <label for="1">Ordinaria</label>
</div>

<div>
  <input type="radio" id="dewey" name="2" value="2">
  <label for="2">Semigratuita</label>
</div>

<div>
  <input type="radio" id="louie" name="3" value="3">
  <label for="3">Gratuita</label>
</div>

<?= form_input('matrícula',set_value('matrícula'),['placeholder'=>"matrícula",  'class'=>"form-control" ])?>           
	</div>     
      
  </div>
  <div class="boton">
	<div>
  	<?= form_submit('boton','Enviar',['class'=>"btn btn-primary"]) ?>
	</div>

    
</html>
