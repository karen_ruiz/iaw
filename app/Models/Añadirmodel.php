<?php

namespace App\Models;

use CodeIgniter\Model;

class Añadirmodel extends Model {
    
    protected $table = 'pau';
    protected $allowedFields = ['NIF','nombre','apellido1','apellido2','email','ciclo'];
    protected $validationRules    = [
        'NIF' => 'required|max_length[9]|regex_match[/^[0-9XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKE]$/]|is_unique[alumnos.nif]',
        'nombre' => 'required|min_length[1]',
        'apellido1' => 'required|min_length[1]',
        'apellido2' => 'permit_empty',
        'email' => 'required|valid_email',
        'ciclo' => 'required|min_length[1]'
  ];
}
